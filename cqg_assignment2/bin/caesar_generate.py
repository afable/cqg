#!/usr/bin/python
 
import sys
import os
import file_util
import caesar_util
 
template_string = '''\
product_family = 'caesar'
question_type = 'caesar'
 
question_text = $q
hotspots = $h
plain_text = "$p"
cipher_text = "$i"
correct_answer = $c
'''
 
if len(sys.argv) != 3:
    print 'Usage: caesar_generate.py template question_directory'
    sys.exit(1)
 
try:
    template = file_util.dynamic_import(sys.argv[1])
except ImportError:
    print 'Failure while importing',sys.argv[1]
    sys.exit(2)
 
for group in template.group_list:
    prefix = group[0]
    question_id = 0
     
    for (plain_text, key, hotspots) in group[1:]:
		question_text = "Use a <b>caesar</b> cipher with key %d to encrypt the plain text." % key
		cipher_text = caesar_util.caesar_encrypt(plain_text, key)
		correct_answer = [ str(cipher_text[i]) for i in range(len(cipher_text)) if i in hotspots ]
		#correct_answer = [ str(i) for i in cipher_text ]
		path = os.path.join(sys.argv[2],prefix+str(question_id))
 
		if not os.path.exists(path):
			os.mkdir(path)

		config_string = template_string
		config_string = config_string.replace('$q',
		 "r'''"+question_text+"'''")
		config_string = config_string.replace('$h',str(hotspots))
		config_string = config_string.replace('$p', plain_text)
		config_string = config_string.replace('$i', cipher_text)
		config_string = config_string.replace('$c', str(correct_answer))

		file_util.write_string(
		 os.path.join(path,'cqg_config.py'), config_string)

		question_id += 1
