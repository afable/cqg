#!/usr/bin/python

import os
import sys
import verify_util
import file_util

# list of verification identifier/condition pairs
condition_list = [
	['group_list_defined',
		"'group_list' is defined"],
	['group_list_list',
		"'group_list' is a list"],
	['group_format',
		'Each group is a list of length at least 2'],
	['group_unique',
		'group[0] is a unique string'],
	['question_format',
		'Each question is a list of length 3'],
	['plaintext',
		"'plaintext' is a string comprising only characters in [A-Z]"],
	['key',
		"'key' is a positive integer less than 26"],
	['hotspots',
		"'hotspots' is a list of indeces into plaintext (0-relative)"],
]
 
def verify_conditions(template,condition_list):
	conditions_dictionary = dict(condition_list)

	if not ('group_list' in dir(template)):
		return conditions_dictionary['group_list_defined']
	if not (type(template.group_list) == list):
		return conditions_dictionary['group_list_type']

	prefix_list = []
	for group in template.group_list:
		if not (type(group) == list and len(group) >= 2):
			return conditions_dictionary['group_format'] + \
			 '\n\tgroup: ' + str(group)

		if not (type(group[0]) == str and group[0] not in prefix_list):
			return conditions_dictionary['group_unique'] + \
			 '\n\tgroup: ' + str(group)
		else:
			prefix_list.append(group[0])

		for question in group[1:]:
			if not (type(question) == list and len(question) == 3):
				return \
				 conditions_dictionary['question_format'] + \
				 '\n\tquestion: ' + str(question)

			if not (type(question[0]) == str): #plaintext is not a string
				return conditions_dictionary['plaintext']+\
				 '\n\tquestion: ' + str(question)
			else:
				for i in question[0]: #plaintext is a string
					if ord(i) not in range(65, 91): #only A-Z char?
						return conditions_dictionary['plaintext']+\
				 			'\n\tquestion: ' + str(question)

			if not (type(question[1]) is int): #key is not an int
				return conditions_dictionary['key'] + \
				 '\n\tquestion: ' + str(question)
			elif question[1] not in range(26): #is int in [0..26)
				return conditions_dictionary['key'] + \
				 '\n\tquestion: ' + str(question)

			if type(question[2]) is list: #is a list
				for i in question[2]:
					if not (type(i) is int): #elem not an int
						return \
						 conditions_dictionary['hotspots'] + \
						 '\n\tquestion: ' + str(question)

					if i not in range(len(question[0])): #elem index not in plaintext
						return \
						 conditions_dictionary[ 
						 'hotspots'] + \
						 '\n\tquestion: '+str(question)
			else: #is not a list
				return \
				 conditions_dictionary['hotspots'] + \
				 '\n\tquestion: ' + str(question)

# handle invocation from command line
if __name__ == '__main__':
	n = verify_util.template_verifier_main(sys.argv,
	 'Question Type caesar: Template Verification Conditions',
	 condition_list,verify_conditions)

	if n == verify_util.COMMAND_LINE_SYNTAX_FAILURE:
		print 'Usage: caesar_template_verifier.py [template_file]'

	sys.exit(n)
