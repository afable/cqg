'''
purpose
    encrypt P using Caesar cipher with key K
preconditions
    P: string of A..Z
    K in 0..25
'''
def caesar_encrypt(P,K):
    alphabet='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    encrypted = ''
    for ltr in P:
        new_index = ( alphabet.index(ltr) + K ) % 26
        encrypted += alphabet[new_index]
    return encrypted
