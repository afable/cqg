import os
import file_util
import html_util

class caesar:
	def __init__(self,question_library_path,question_path):
		config = file_util.dynamic_import(os.path.join(question_library_path,question_path,'cqg_config.py'))
		self.question_library_path = question_library_path
		self.question_path = question_path

		self.question_text = config.question_text
		self.hotspots = config.hotspots
		self.plain_text = config.plain_text
		self.cipher_text = config.cipher_text
		self.correct_answer = config.correct_answer

	def get_question_library_path(self):
		print "qet_question_library_path"
		return self.question_library_path
     
	def get_question_path(self):
		print "qet_question_path"
		return self.question_path

	def get_css(self,answer):
		print "get_css"
		return style
    
	def get_input_element_ids(self):
		print "get_input_element_ids"
		return ['answer']  

	def get_html(self,answer):
		print "get_html"
		print "answer:", answer
		html = "<div>"
		html += "<p>" + self.question_text + "</p>"
		
		html += "<center><table cellspacing=0 cellpadding=3>\n"
		html += "<tr><td class='right'>plain text</td>"
		for ltr in self.plain_text:
			html += "<td class='left_border center'><tt>%s</tt></td>" % ltr
		html += "</tr>\n<tr><td class='top_border right' >cipher text</td>"
		
		inputs = []
		if type(answer['answer']) is list:
			for i in range(len(answer['answer'])):
				inputs.append(html_util.get_text('char_%i' % i, answer['answer'][i], 1)) #name, value, size
		
		for i in range(len(self.cipher_text)):
			if i in self.hotspots:
				if len(inputs) > 0: 
					html += "<td class='left_border top_border'><tt>%s</tt></td>" % inputs[self.hotspots.index(i)] 
				else:
					html += "<td class='left_border top_border'><tt>%s</tt></td>" % html_util.get_text('char_%i' % i, '', 1)
			else:
				html += "<td class='left_border top_border'><tt>%s</tt></td>" % self.cipher_text[i]
			 
		html += "</tr>\n</table></center>"
		print html
		return html + "</div>"

	def check_answer(self,answer):
		print "check_answer:", answer
		try:
			answer_list = [str(i) for i in answer['answer']]
			print "answer_list", answer_list
			print "self.correct_answer", self.correct_answer
			return set(answer_list) == set(self.correct_answer)
		except:
			print "check_answer exception"
			return False
 
style = '''
    #question_cell div {
        text-align: center;
        width:75%;
        margin:auto;
    }
    #question_cell table, #question_cell td {
        border:0px;
    }
    td.top {
        vertical-align:top;
    }
    td.left {
        text-align:left;
    }
	td.right { text-align: right;}
	td.center { text-align: center;}
	td.top_border { border-top: 1px solid black !important}
	td.left_border { border-left: 1px solid black !important }
	td.right_border { border-right: 1px solid black !important }
	td.bottom_border { border-bottom: 1px solid black !important }
	td.all_borders { border: 1px solid black; }
'''
