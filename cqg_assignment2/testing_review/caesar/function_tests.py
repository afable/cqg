#!/usr/bin/python
'''
Description:
  function.tests.py provides automated tests for the caesar_encrypt function \
  in caesar_util. We test 3 variable iterations so that we cover:
    1. Plaintext with lengths 0, 1, 2, 3, and 10.
    2. Key values 0, 1, 24, and 25.
    3. (a) plaintext containing only 'A', 'B', 'Y', and 'Z' and (b) plaintext \
         containing only 'C'..'X'. 
'''

from caesar_util import caesar_encrypt

## cover 3 variables of testing iterations
lengths = [0, 1, 2, 3, 10]
keys = [0, 1, 24, 25]
plaintexts = ['ABYZ', 'CDEFGHIJKLMNOPQRSTUVWX']

## alphabet used in getting the correct cipher value to test against function
alphabet='ABCDEFGHIJKLMNOPQRSTUVWXYZ'

## test implementation
for length in lengths:
	for key in keys:
		for plaintext in plaintexts:
			## set plaintext
			P = plaintext[:length]

			## set correct cipher text according to key 
			C_correct = ""
			for letter in P:
				c_i = ( alphabet.index(letter) + key ) % 26
				C_correct += alphabet[c_i]

			## test if correct cipher text matches actual returned cipher text
			C = caesar_encrypt(P, key)
			if C_correct != C:
				print 'Failure with inputs (%s, %i) and output: %s. Expected output: %s' %(P, key, C, C_correct)

